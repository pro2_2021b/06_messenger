package apiModel;

public class UserMembership
{
    private String name;
    private int id;
    private boolean isPublic;

    public UserMembership(String name, int id, boolean isPublic) {
        this.name = name;
        this.id = id;
        this.isPublic = isPublic;
    }

    public String getName()
    {
        return name;
    }

    public int getId()
    {
        return id;
    }
}

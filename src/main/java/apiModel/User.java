package apiModel;

import java.util.ArrayList;

public class User
{
    private int id;
    private String name;
    private String address;
    private String email;
    private String phone;

    public User(int id, String name, String address, String email, String phone, UserMembership[] groups) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.email = email;
        this.phone = phone;
        this.groups = groups;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }

    public UserMembership[] getGroups() {
        return groups;
    }

    private UserMembership[] groups;
}

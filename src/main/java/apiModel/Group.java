package apiModel;

import java.time.*;
import java.util.ArrayList;

public class Group
{
    private String name;
    private int id;
    private boolean isPublic;
    private LocalDateTime createdDatetime;
    private GroupMember[] members;

    public Group(String name, int id, boolean isPublic, LocalDateTime createdDatetime, GroupMember[] members) {
        this.name = name;
        this.id = id;
        this.isPublic = isPublic;
        this.createdDatetime = createdDatetime;
        this.members = members;
    }

    public String getName()
    {
        return name;
    }

    public int getId()
    {
        return id;
    }

    public boolean isPublic()
    {
        return isPublic;
    }

    public LocalDateTime getCreatedDatetime()
    {
        return createdDatetime;
    }

    public GroupMember[] getMembers()
    {
        return members;
    }
}

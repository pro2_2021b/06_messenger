package apiModel;

import java.time.*;

public class Message
{
    private int id;
    private String text;
    private User author;
    private long groupId;
    private Instant dateTime;

    public Message(int id, String text, int groupId, User author, Instant dateTime) {
        this.id = id;
        this.text = text;
        this.dateTime = dateTime;
        this.author = author;
        this.groupId = groupId;
    }

    public int getId()
    {
        return id;
    }

    public String getText()
    {
        return text;
    }

    public User getAuthor()
    {
        return author;
    }

    public long getGroupId()
    {
        return groupId;
    }

    public Instant getDateTime()
    {
        return dateTime;
    }
}
package apiModel;

public class GroupMember
{
    private int id;
    private String name;

    public GroupMember(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId()
    {
        return id;
    }

    public String getName()
    {
        return name;
    }
}

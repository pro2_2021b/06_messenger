package dbModel;

import java.sql.*;
import java.util.ArrayList;

public class MsSqlServerMessengerDao implements MessengerDao {

    PreparedStatement getUserPreparedStatement;
    PreparedStatement getGroupsPreparedStatement;
    PreparedStatement getGroupPreparedStatement;
    PreparedStatement getMessagesPreparedStatement;

    Connection connection;
    public MsSqlServerMessengerDao(String url, String user, String password) throws SQLException, ClassNotFoundException {
        // Tady musí být třída (driver) SQLServerDriver JAKKOLIV použitá
        Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
        connection = DriverManager.getConnection(url, user, password);
        getUserPreparedStatement = connection.
                prepareStatement("select * from MessengerUser where id=?");
        getGroupPreparedStatement = connection.
                prepareStatement("select * from MessengerGroup where id=?");
        getGroupsPreparedStatement = connection.
                prepareStatement("select mg.Name mgName, mg.ID mgId, mg.[Public] mgPublic from MessengerUserMessengerGroup mumg " +
                        "  join MessengerGroup mg on mumg.messengerGroupId = mg.id "+
                        " where mumg.messengerUserId=?");
        getMessagesPreparedStatement = connection.
                prepareStatement("select m.ID mID, mu.ID muID, * from Message m join MessengerUser mu on m.FromPersonID=mu.ID where m.GroupId=?");
    }

    @Override
    public MessengerGroup GetGroup(long id)
    {
        MessengerGroup result;
        try(Statement statement = connection.createStatement())
        {
            getGroupPreparedStatement.setLong(1, id);
            ResultSet resultSet= getGroupPreparedStatement.executeQuery();
            resultSet.next();
            result = new MessengerGroup(
                    resultSet.getLong("ID"),
                    resultSet.getString("Name"),
                    resultSet.getBoolean("Public")
            );
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            return null;
        }
        return result;
    }

    @Override
    public MessengerUser GetUser(long id) {

        MessengerUser result;
        try(Statement statement = connection.createStatement())
        {
            getUserPreparedStatement.setLong(1, id);
            ResultSet resultSet= getUserPreparedStatement.executeQuery();
            resultSet.next();
            result = new MessengerUser(
                    resultSet.getString("Name"),
                    resultSet.getString("Address"),
                    resultSet.getString("Email"),
                    resultSet.getString("Phone"),
                    resultSet.getLong("ID")
            );
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            return null;
        }
        return result;
    }

    @Override
    public MessengerUser[] GetMembers(MessengerGroup group) {

        return new MessengerUser[0];
    }

    @Override
    public MessengerGroup[] GetGroups(MessengerUser user) {

        ArrayList<MessengerGroup> result = new ArrayList<>();
        try
        {
            getGroupsPreparedStatement.setLong(1,user.getId());
            ResultSet resultSet= getGroupsPreparedStatement.executeQuery();
            while(resultSet.next())
            {
                MessengerGroup messengerGroup = new MessengerGroup(
                        resultSet.getLong("mgID"),
                        resultSet.getString("mgName"),
                        resultSet.getBoolean("mgPublic")
                );
                result.add(messengerGroup);
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            return null;
        }
        return result.toArray(new MessengerGroup[0]);
    }

    @Override
    public Message[] GetMessages(MessengerGroup group) {

        ArrayList<Message> results = new ArrayList<>();
        try(Statement statement = connection.createStatement())
        {
            getMessagesPreparedStatement.setLong(1, group.getId());
            ResultSet resultSet= getMessagesPreparedStatement.executeQuery();
            while (resultSet.next())
            {
                MessengerUser from = new MessengerUser(
                        resultSet.getString("Name"),
                        resultSet.getString("Address"),
                        resultSet.getString("Email"),
                        resultSet.getString("Phone"),
                        resultSet.getLong("muID")
                        );

                Message message = new Message();
                message.setId(resultSet.getLong("mID"));
                message.setText(resultSet.getString("Text"));
                message.setTime(resultSet.getTimestamp("Time").toInstant());
                message.setUser(from);
                message.setGroup(group);
                results.add(message);
            }

        }
        catch (SQLException e)
        {
            e.printStackTrace();
            return null;
        }
        return results.toArray(new Message[0]);
    }
}

package dbModel;

public class MessengerGroup {
    private long id;
    private String name;
    private boolean isPublic;

    public long getId()
    {
        return id;
    }

    public String getName()
    {
        return name;
    }

    public boolean isPublic()
    {
        return isPublic;
    }

    public MessengerGroup(long id, String name, boolean isPublic)
    {
        this.id = id;
        this.name = name;
        this.isPublic = isPublic;
    }
}

package dbModel;

public interface MessengerDao
{
    MessengerGroup GetGroup(long id);
    MessengerUser GetUser(long id);
    MessengerUser[] GetMembers(MessengerGroup group);
    MessengerGroup[] GetGroups(MessengerUser user);
    Message[] GetMessages(MessengerGroup group);
}

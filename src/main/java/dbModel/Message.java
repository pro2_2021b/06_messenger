package dbModel;

import java.time.Instant;

public class Message
{
    private Instant time;
    private String text;
    private MessengerUser user;
    private MessengerGroup group;
    private long id;

    public Instant getTime() {
        return time;
    }

    public void setTime(Instant time) {
        this.time = time;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public MessengerUser getUser() {
        return user;
    }

    public void setUser(MessengerUser user) {
        this.user = user;
    }

    public MessengerGroup getGroup() {
        return group;
    }

    public void setGroup(MessengerGroup group) {
        this.group = group;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}

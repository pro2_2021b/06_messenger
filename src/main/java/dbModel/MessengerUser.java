package dbModel;

public class MessengerUser {
    private String name;
    private String address;
    private String email;
    private String phone;
    private long id;

    public MessengerUser(String name, String address, String email, String phone, long id)
    {
        this.name = name;
        this.address = address;
        this.email = email;
        this.phone = phone;
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public String getAddress()
    {
        return address;
    }

    public String getEmail()
    {
        return email;
    }

    public String getPhone()
    {
        return phone;
    }

    public long getId()
    {
        return id;
    }
}

package server;

import apiModel.*;
import apiModel.Message;
import com.google.gson.*;
import dbModel.*;

import java.io.*;
import java.lang.reflect.Type;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.SQLException;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;

public class ServerApp
{
    private static void ReturnUser(String s, PrintWriter printWriter, MessengerDao messengerDao)
    {
        long userId = Long.parseLong(s);
        MessengerUser messengerUser = messengerDao.GetUser(userId);
        MessengerGroup[] groups = messengerDao.GetGroups(messengerUser);
        UserMembership[] apiMemberships = new UserMembership[groups.length];
        for(int i=0; i< groups.length; i++)
        {
            apiMemberships[i] = new UserMembership(groups[i].getName(), (int)groups[i].getId(), groups[i].isPublic() );
        }
        User user = new User((int)userId,messengerUser.getName(),messengerUser.getAddress(), messengerUser.getEmail(), messengerUser.getPhone(), apiMemberships);
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();
        String result = gson.toJson(user);
        printWriter.println(result);
    }

    private static void ReturnGroup(String s, PrintWriter printWriter, MessengerDao messengerDao)
    {

    }

    private static void ReturnGroupMessages(String s, PrintWriter printWriter, MessengerDao messengerDao)
    {
        long groupId = Long.parseLong(s);
        MessengerGroup dbGroup = messengerDao.GetGroup(groupId);
        dbModel.Message[] dbMessages = messengerDao.GetMessages(dbGroup);

        apiModel.Message[] apiMessages = new Message[dbMessages.length];
        for(int i=0;i<dbMessages.length;i++)
        {
            MessengerUser from = dbMessages[i].getUser();
            apiMessages[i] = new Message(
                    (int) dbMessages[i].getId(),
                    dbMessages[i].getText(),
                    (int)dbMessages[i].getGroup().getId(),
                    new User((int)from.getId(),from.getName(),from.getAddress(), from.getEmail(), from.getPhone(), null),
                    dbMessages[i].getTime());
        }
        GsonBuilder gsonBuilder = new GsonBuilder().registerTypeAdapter(Instant.class, new JsonSerializer() {

            @Override
            public JsonElement serialize(Object o, Type type, JsonSerializationContext jsonSerializationContext)
            {
                return new JsonPrimitive(ZonedDateTime.ofInstant((Instant)o, ZoneId.systemDefault()).toString());
            }
        });
        Gson gson = gsonBuilder.create();
        String result = gson.toJson(apiMessages);
        printWriter.println(result);
    }
    public static void Run()
    {
        ServerSocket serverSocket = null;
        try
        {
            serverSocket = new ServerSocket(180);
            while (true)
            {
                Socket socketConnection = serverSocket.accept();
                Thread thread = new Thread(){
                    @Override
                    public void run()
                    {
                        try
                        {
                            MsSqlServerMessengerDao msSqlMessengerDataManager = new MsSqlServerMessengerDao(
                                    "jdbc:sqlserver://database-1.cwnuam1akix6.us-east-2.rds.amazonaws.com:1194;DatabaseName=PRO2b_Messenger",
                                    "PRO2Students",
                                    "3dw84fr-4354.o+4");
                            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(socketConnection.getInputStream()));
                            PrintWriter printWriter = new PrintWriter(socketConnection.getOutputStream());
                            String firstLine = bufferedReader.readLine();
                            String[] split = firstLine.split(" ");
                            String[] splitAddress = split[1].split("/");
                            printWriter.println("HTTP/1.1 200 OK");
                            printWriter.println();
                            GsonBuilder gsonBuilder = new GsonBuilder();
                            Gson gson = gsonBuilder.create();
                            if(splitAddress[1].equals("groups"))
                            {
                                if(splitAddress.length >= 4  && splitAddress[3].equals("messages"))
                                {
                                    ReturnGroupMessages(splitAddress[2], printWriter, msSqlMessengerDataManager);
                                }
                                else
                                {
                                    ReturnGroup(splitAddress[2], printWriter, msSqlMessengerDataManager);
                                }
                            }
                            else if(splitAddress[1].equals("users"))
                            {
                                ReturnUser(splitAddress[2], printWriter, msSqlMessengerDataManager);
                            }
                            printWriter.flush();
                            printWriter.close();
                            socketConnection.close();
                        } catch (IOException | SQLException | ClassNotFoundException e) {
                            e.printStackTrace();
                        }
                    }
                };
                thread.start();
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
}

package experiments;

import client.gui.ClientMainFrame;
import server.ServerApp;

import javax.swing.*;
import java.io.IOException;

public class TestClass
{
    public static void main(String[] args)
    {
        Thread serverThread = new Thread(() -> ServerApp.Run());
        serverThread.start();

        SwingUtilities.invokeLater(() -> {
            try {
                new ClientMainFrame();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }
}

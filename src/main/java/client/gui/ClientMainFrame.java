package client.gui;
import apiModel.*;
import com.google.gson.*;
import shared.*;
import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.time.Instant;
import java.time.ZonedDateTime;

public class ClientMainFrame extends JFrame
{
    JTextPane userTextPane;
    MembershipsTableModel membershipsTableModel;
    MessagesTableModel messagesTableModel;

    public ClientMainFrame() throws IOException {
        setTitle("Time server");
        setVisible(true);
        setSize(600, 800);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        userTextPane = new JTextPane();
        setLayout(new BorderLayout());
        JPanel topPanel = new JPanel();
        JPanel centerPanel = new JPanel(new GridLayout());
        add(topPanel, BorderLayout.NORTH);
        add(centerPanel, BorderLayout.CENTER);
        topPanel.setLayout(new BoxLayout(topPanel,BoxLayout.X_AXIS));
        JTextField user = new JTextField();
        JButton loadButton = new JButton("PŘIHLÁSIT");
        topPanel.add(new JLabel("User ID:"));
        topPanel.add(user);
        topPanel.add(loadButton);
        loadButton.addActionListener(e -> {
            new Thread(() -> {
                LoadAll(user.getText());
            }).start();
        });
        JTabbedPane tabbedPane = new JTabbedPane();
        centerPanel.add(tabbedPane);
        JPanel groupsPanel = new JPanel(new GridLayout());
        JPanel plainGroupsPanel = new JPanel(new GridLayout());
        JPanel messagesPanel = new JPanel(new GridLayout());
        plainGroupsPanel.add(userTextPane);

        tabbedPane.addTab("Profil (plain text)", plainGroupsPanel);
        tabbedPane.addTab("Skupiny", groupsPanel);
        tabbedPane.addTab("Zprávy", messagesPanel);

        membershipsTableModel = new MembershipsTableModel();
        JTable membershipsTable = new JTable(membershipsTableModel);
        groupsPanel.add(membershipsTable);

        membershipsTable.getSelectionModel().
                addListSelectionListener(event ->
                        LoadMessages(membershipsTable.getValueAt(membershipsTable.getSelectedRow(), 1) ));

        messagesTableModel = new MessagesTableModel();
        JTable messagesTable = new JTable(messagesTableModel);
        messagesPanel.add(messagesTable);
    }

    private void LoadMessages(Object groupIdObject)
    {
        try
        {
            String json = Utils.GetContentFromURL("http://localhost:180/groups/"+(int)groupIdObject+"/messages/");

            GsonBuilder gsonBuilder = new GsonBuilder().registerTypeAdapter(Instant.class, new JsonDeserializer<Instant>() {
                @Override
                public Instant deserialize(JsonElement jsonElement, java.lang.reflect.Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException
                {
                    return ZonedDateTime.parse(jsonElement.getAsJsonPrimitive().getAsString()).toInstant();
                }
            });
            Gson gson = gsonBuilder.create();
            SwingUtilities.invokeLater(()->{
                messagesTableModel.set(gson.fromJson(json, Message[].class));
            });
        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }


    private void LoadAll(String id)
    {
        try
        {
            String exampleJson = Utils.GetContentFromURL("http://localhost:180/users/"+id);
            SwingUtilities.invokeLater(()->{
                userTextPane.setText(exampleJson);
            });
            GsonBuilder gsonBuilder = new GsonBuilder().registerTypeAdapter(Instant.class, new JsonDeserializer<Instant>() {
                @Override
                public Instant deserialize(JsonElement jsonElement, java.lang.reflect.Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException
                {
                    return ZonedDateTime.parse(jsonElement.getAsJsonPrimitive().getAsString()).toInstant();
                }
            });
            Gson gson = gsonBuilder.create();
            SwingUtilities.invokeLater(()->{
                membershipsTableModel.set(gson.fromJson(exampleJson, User.class).getGroups());
            });
            } catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}